package com.openclassrooms.mddapi.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;

import com.openclassrooms.mddapi.config.EncoderConfig;
import com.openclassrooms.mddapi.config.JwtTokenUtil;
import com.openclassrooms.mddapi.config.UserDetailsServiceImpl;
import com.openclassrooms.mddapi.dto.*;
import com.openclassrooms.mddapi.models.Topic;
import com.openclassrooms.mddapi.models.User;
import com.openclassrooms.mddapi.services.UserService;

@RestController
@RequestMapping("api/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authManager;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private EncoderConfig encoderConfig;

    /**
     * POST request /api/auth/login
     * With an email and a password in body (valid login dto), if correct credentials get JWT token (login response)
     * @param loginDto LoginDto (email and password)
     * @return response entity ResponseEntity (login response LoginResponse)
     * @throws Exception
     */
    @PostMapping(path="/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginDto loginDto) throws Exception {

        return this.authenticate(loginDto.getEmail(), loginDto.getPassword());
    }

    /**
     * POST request /api/auth/register
     * With a username, an email and a password in body (valid register dto), create a new user and get message (register response)
     * @param registerDto RegisterDto (email, username and password)
     * @return response entity ResponseEntity (register response RegisterResponse)
     */
    @PostMapping(path="/register")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterDto registerDto) {

        if (userService.exists(registerDto.getEmail())) {
            return ResponseEntity.badRequest().body(new RegisterResponse("Error: Email is already taken !"));
        }

        PasswordEncoder encoder = encoderConfig.passwordEncoder();
        LocalDateTime now = LocalDateTime.now();

        User user = new User(
            null,
            registerDto.getEmail(),
            registerDto.getUsername(),
            encoder.encode(registerDto.getPassword()),
            now, now, new ArrayList<>()
        );

        userService.createUser(user);

        return ResponseEntity.ok(new RegisterResponse("User registered successfully !"));
    }

    /**
     * GET request /api/auth/me
     * With authentication, get id, email, username, password, createdAt, updatedAt and topics of logged user (me response)
     * @param authentication Authentication
     * @return me response MeResponse (user id, email, username, password, createdAt, updatedAt and topics)
     */
    @GetMapping(path="/me")
    public MeResponse me(Authentication authentication) {
        Optional<User> user = userService.getUser(authentication.getName());
        Long id = user.orElseThrow().getId();
        String email = user.get().getEmail();
        String username = user.get().getUsername();
        String password = user.get().getPassword();
        LocalDateTime createdAt = user.get().getCreatedAt();
        LocalDateTime updatedAt = user.get().getUpdatedAt();
        List<Topic> topics = user.get().getTopics();

        return new MeResponse(id, email, username, password, createdAt, updatedAt, topics);
    }

    /**
     * With an email and a password, if correct credentials generate and get JWT token (login response)
     * @param email String
     * @param password String
     * @return response entity ResponseEntity (login response LoginResponse)
     * @throws Exception
     */
    private ResponseEntity<?> authenticate(String email, String password) throws Exception {

        try {
            this.authManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new LoginResponse(token));
    }
}
