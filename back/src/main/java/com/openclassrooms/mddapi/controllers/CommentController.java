package com.openclassrooms.mddapi.controllers;

import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;

import com.openclassrooms.mddapi.dto.CommentDto;
import com.openclassrooms.mddapi.dto.CommentResponse;
import com.openclassrooms.mddapi.dto.CommentsResponse;
import com.openclassrooms.mddapi.models.Comment;
import com.openclassrooms.mddapi.services.CommentService;

@RestController
@RequestMapping("api/comments")
public class CommentController {

    @Autowired
    private CommentService commentService;

    /**
     * POST request /api/comments
     * With a content, a post and a user in body (valid comment dto), create a new comment and get message (comment response)
     * @param commentDto CommentDto (content, post and user)
     * @return commentResponse CommentResponse (message)
     */
    @PostMapping(path="")
    public CommentResponse commentCreate(@Valid @RequestBody CommentDto commentDto) {
        LocalDateTime now = LocalDateTime.now();
        Comment comment = new Comment(null, commentDto.getContent(), commentDto.getPost(), commentDto.getUser(), now, now);

        Comment createdComment = this.commentService.createComment(comment);

        CommentResponse response;
        if(createdComment!=null && createdComment.getId()>0) {
            response = new CommentResponse("Saved.");
        } else {
            response = new CommentResponse("Unsaved.");
        }
        return response;
    }

    /**
     * GET request /api/comments/post/{id}
     * With a path post id, get comments list of associated post
     * @param id Long
     * @return commentsResponse CommentsResponse (comments list)
     */
    @GetMapping(path="/post/{id}")
    public CommentsResponse commentsByPost(@PathVariable Long id) {
        return this.commentService.getByPostId(id);
    }
}
