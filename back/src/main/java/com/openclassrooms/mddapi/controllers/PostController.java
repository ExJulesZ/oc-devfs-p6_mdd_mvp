package com.openclassrooms.mddapi.controllers;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;

import com.openclassrooms.mddapi.dto.PostDto;
import com.openclassrooms.mddapi.dto.PostResponse;
import com.openclassrooms.mddapi.dto.PostsResponse;
import com.openclassrooms.mddapi.models.Post;
import com.openclassrooms.mddapi.services.PostService;

@RestController
@RequestMapping("api/posts")
public class PostController {

    @Autowired
    private PostService postService;

    /**
     * POST request /api/posts
     * With a title, a content, a topic and a user in body (valid post dto), create a new post and get message (post response)
     * @param postDto PostDto (title, content, topic and user)
     * @return postResponse PostResponse (message)
     */
    @PostMapping(path="")
    public PostResponse postCreate(@Valid @RequestBody PostDto postDto) {
        LocalDateTime now = LocalDateTime.now();
        Post post = new Post(null, postDto.getTitle(), postDto.getContent(), postDto.getTopic(), postDto.getUser(), now, now);

        Post createdPost = this.postService.createPost(post);

        PostResponse response;
        if(createdPost!=null && createdPost.getId()>0) {
            response = new PostResponse("Saved.");
        } else {
            response = new PostResponse("Unsaved.");
        }
        return response;
    }

    /**
     * GET request /api/posts
     * Get all posts list
     * @return postsResponse postsResponse (posts list)
     */
    @GetMapping(path="")
    public PostsResponse postGetAll() {
        return this.postService.getPosts();
    }

    /**
     * GET request /api/posts/{id}
     * With a path post id, if exists get associated post
     * @param id Long
     * @return post Optional<Post>
     */
    @GetMapping(path="/{id}")
    public Optional<Post> postGet(@PathVariable Long id) {
        return this.postService.getPost(id);
    }
}
