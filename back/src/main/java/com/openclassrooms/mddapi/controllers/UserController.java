package com.openclassrooms.mddapi.controllers;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;

import com.openclassrooms.mddapi.dto.UserDto;
import com.openclassrooms.mddapi.dto.UserResponse;
import com.openclassrooms.mddapi.models.User;
import com.openclassrooms.mddapi.services.UserService;

@RestController
@RequestMapping("api/users")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * PUT request /api/users/{id}
     * With a path user id, if user exists update it and get message (user response)
     * @param id Long
     * @param userDto UserDto (email and username)
     * @return userResponse UserResponse (message)
     */
    @PutMapping(path="/{id}")
    public UserResponse userUpdate(@PathVariable Long id, @RequestBody @Valid UserDto userDto) {

        Optional<User> currentUser = userService.getUser(id);
        if(currentUser.isPresent()) {

            User user = new User(
                id,
                userDto.getEmail(),
                userDto.getUsername(),
                currentUser.get().getPassword(),
                currentUser.get().getCreatedAt(),
                LocalDateTime.now(),
                currentUser.get().getTopics()
            );

            userService.updateUser(user);

            return new UserResponse("Updated.");
        } else {
            return new UserResponse("Unupdated : no user with id "+id.toString()+".");
        }
    }

    /**
     * POST request /api/users/{userId}/topic/{topicId}/subscribe
     * With a path user id and a path topic id, if user and topic exists and associated subscription doesn't exist create a new subscription
     * @param userId String
     * @param topicId String
     */
    @PostMapping("/{userId}/topic/{topicId}/subscribe")
    public void topicSubscribe(@PathVariable("userId") String userId, @PathVariable("topicId") String topicId) {
        this.userService.subscribe(Long.parseLong(userId), Long.parseLong(topicId));
    }

    /**
     * DELETE request /api/users/{userId}/topic/{topicId}/unsubscribe
     * With a path user id and a path topic id, if user, topic and associated subscription exists delete associated subscription
     * @param userId String
     * @param topicId String
     */
    @DeleteMapping("/{userId}/topic/{topicId}/unsubscribe")
    public void topicUnsubscribe(@PathVariable("userId") String userId, @PathVariable("topicId") String topicId) {
        this.userService.unsubscribe(Long.parseLong(userId), Long.parseLong(topicId));
    }
}
