package com.openclassrooms.mddapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.openclassrooms.mddapi.dto.TopicsResponse;
import com.openclassrooms.mddapi.services.TopicService;

@RestController
@RequestMapping("api/topics")
public class TopicController {

    @Autowired
    private TopicService topicService;

    /**
     * GET request /api/topics
     * Get all topics list
     * @return topicsResponse topicsResponse (topics list)
     */
    @GetMapping(path="")
    public TopicsResponse topicGetAll() {
        return this.topicService.getTopics();
    }
}