package com.openclassrooms.mddapi.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openclassrooms.mddapi.dto.PostsResponse;
import com.openclassrooms.mddapi.models.Post;
import com.openclassrooms.mddapi.repositories.PostRepository;

@Service
public class PostService {

    @Autowired
    private PostRepository postRepository;

    /**
     * CRUD function
     * With a post object, create associated new post
     * @param post Post
     * @return created post object Post
     */
    public Post createPost(final Post post) {
        return this.postRepository.save(post);
    }

    /**
     * CRUD function
     * Get list of all existant posts (posts response)
     * @return posts response (posts list) PostsResponse
     */
    public PostsResponse getPosts() {
        return new PostsResponse(this.postRepository.findAll());
    }

    /**
     * CRUD function
     * With a post id, if exists get associated post
     * @param id Long
     * @return post object Optional<Post>
     */
    public Optional<Post> getPost(final Long id) {
        return this.postRepository.findById(id);
    }



    // Never used CRUD functions :

    /**
     * CRUD function, never used
     * With a post object, if exists update associated post
     * @param post Post
     */
    public void updatePost(final Post post) {
        this.createPost(post);
    }

    /**
     * CRUD function, never used
     * With a post id, if exists delete associated post
     * @param id Long
     */
    public void deletePost(final Long id) {
        this.postRepository.deleteById(id);
    }
}
