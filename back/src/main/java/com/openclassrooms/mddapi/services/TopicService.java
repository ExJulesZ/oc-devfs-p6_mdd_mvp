package com.openclassrooms.mddapi.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openclassrooms.mddapi.dto.TopicsResponse;
import com.openclassrooms.mddapi.models.Topic;
import com.openclassrooms.mddapi.repositories.TopicRepository;

@Service
public class TopicService {

    @Autowired
    private TopicRepository topicRepository;

    /**
     * CRUD function
     * Get list of all existant topics (topics response)
     * @return topics response (topics list) TopicsResponse
     */
    public TopicsResponse getTopics() {
        return new TopicsResponse(this.topicRepository.findAll());
    }



    // Never used CRUD functions :

    /**
     * CRUD function, never used
     * With a topic object, create associated new topic
     * @param topic Topic
     * @return created topic object Topic
     */
    public Topic createTopic(final Topic topic) {
        return this.topicRepository.save(topic);
    }

    /**
     * CRUD function, never used
     * With a topic id, if exists get associated topic
     * @param id Long
     * @return topic object Optional<Topic>
     */
    public Optional<Topic> getTopic(final Long id) {
        return this.topicRepository.findById(id);
    }

    /**
     * CRUD function, never used
     * With a topic object, if exists update associated topic
     * @param topic Topic
     */
    public void updateTopic(final Topic topic) {
        this.createTopic(topic);
    }

    /**
     * CRUD function, never used
     * With a topic id, if exists delete associated topic
     * @param id Long
     */
    public void deleteTopic(final Long id) {
        this.topicRepository.deleteById(id);
    }
}
