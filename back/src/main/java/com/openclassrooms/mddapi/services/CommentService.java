package com.openclassrooms.mddapi.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openclassrooms.mddapi.dto.CommentsResponse;
import com.openclassrooms.mddapi.models.Comment;
import com.openclassrooms.mddapi.repositories.CommentRepository;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    /**
     * CRUD function
     * With a comment object, create associated new comment
     * @param comment Comment
     * @return created comment object Comment
     */
    public Comment createComment(final Comment comment) {
        return this.commentRepository.save(comment);
    }

    /**
     * Custom function
     * With post id, if exists get associated comments list (comments response)
     * @param id Long
     * @return comments response (comments list) CommentsResponse
     */
    public CommentsResponse getByPostId(final Long id) {
        return new CommentsResponse(this.commentRepository.findByPostId(id));
    }



    // Never used CRUD functions :

    /**
     * CRUD function, never used
     * Get list of all existant comments
     * @return comments list Iterable<Comment>
     */
    public Iterable<Comment> getComments() {
        return this.commentRepository.findAll();
    }

    /**
     * CRUD function, never used
     * With a comment id, if exists get associated comment
     * @param id Long
     * @return comment object Optional<Comment>
     */
    public Optional<Comment> getComment(final Long id) {
        return this.commentRepository.findById(id);
    }

    /**
     * CRUD function, never used
     * With a comment object, if exists update associated comment
     * @param comment Comment
     */
    public void updateComment(final Comment comment) {
        this.createComment(comment);
    }

    /**
     * CRUD function, never used
     * With a comment id, if exists delete associated comment
     * @param id Long
     */
    public void deleteComment(final Long id) {
        this.commentRepository.deleteById(id);
    }
}
