package com.openclassrooms.mddapi.services;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openclassrooms.mddapi.config.UserDetailsServiceImpl;
import com.openclassrooms.mddapi.exceptions.BadRequestException;
import com.openclassrooms.mddapi.exceptions.NotFoundException;
import com.openclassrooms.mddapi.models.Topic;
import com.openclassrooms.mddapi.models.User;
import com.openclassrooms.mddapi.repositories.TopicRepository;
import com.openclassrooms.mddapi.repositories.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    /**
     * CRUD function
     * With a user object, create associated new user
     * @param user User
     * @return created user object User
     */
    public User createUser(final User user) {
        return this.userRepository.save(user);
    }

    /**
     * CRUD function
     * With a user id, if exists get associated user
     * @param id Long
     * @return user object Optional<User>
     */
    public Optional<User> getUser(final Long id) {
        return this.userRepository.findById(id);
    }

    /**
     * Custom function
     * With a user email, if exists get associated user
     * @param email String
     * @return user object Optional<User>
     */
    public Optional<User> getUser(String email) {
        return this.userRepository.findByEmail(email);
    }

    /**
     * Custom function
     * With user email, return true if user exists, else return false
     * @param email String
     * @return user exists Boolean
     */
    public Boolean exists(String email) {
        return this.userRepository.existsByEmail(email);
    }

    /**
     * CRUD function
     * With a user object, if exists update associated user
     * @param user User
     */
    public void updateUser(final User user) {
        this.createUser(user);
    }

    /**
     * Custom function
     * With a user id and a topic id, if user and topic exists and associated subscription doesn't exist create a new subscription
     * @param userId Long
     * @param topicId Long
     */
    public void subscribe(Long userId, Long topicId) {
        User user = this.userRepository.findById(userId).orElse(null);
        Topic topic = this.topicRepository.findById(topicId).orElse(null);

        if(user == null || topic == null) {
            throw new NotFoundException();
        }
        boolean alreadySubscribe = user.getTopics().stream().anyMatch(item -> item.getId().equals(topicId));
        if(alreadySubscribe) {
            throw new BadRequestException();
        }

        user.getTopics().add(topic);
        this.userRepository.save(user);
    }

    /**
     * Custom function
     * With a user id and a topic id, if user, topic and associated subscription exists delete associated subscription
     * @param userId Long
     * @param topicId Long
     */
    public void unsubscribe(Long userId, Long topicId) {
        User user = this.userRepository.findById(userId).orElse(null);

        if(user == null) {
            throw new NotFoundException();
        }
        boolean alreadySubscribe = user.getTopics().stream().anyMatch(item -> item.getId().equals(topicId));
        if(!alreadySubscribe) {
            throw new BadRequestException();
        }

        user.setTopics(user.getTopics().stream().filter(item -> !item.getId().equals(topicId)).collect(Collectors.toList()));
        this.userRepository.save(user);
    }



    // Never used CRUD functions :

    /**
     * CRUD function, never used
     * Get list of all existant users
     * @return users list Iterable<User>
     */
    public Iterable<User> getUsers() {
        return this.userRepository.findAll();
    }

    /**
     * CRUD function, never used
     * With a user id, if exists delete associated user
     * @param id Long
     */
    public void deleteUser(final Long id) {
        this.userRepository.deleteById(id);
    }
}
