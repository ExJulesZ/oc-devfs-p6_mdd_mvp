package com.openclassrooms.mddapi.config;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Builder
@AllArgsConstructor
@Getter
public class UserDetailsImpl implements UserDetails {

    private Long id;

    private String username; // user email

    private String name; // user username

    @JsonIgnore
    private String password;

    /**
     * get user authorities, but user doesn't have special authorities in this api
     * @return authorities Collection< ? extends GrantedAuthority>
     */
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new HashSet<GrantedAuthority>();
    }

    /**
     * UserDetails function
     * return true if user account is not expired, always true
     * @return not expired Boolean (true)
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * UserDetails function
     * return true if user account is not locked, always true
     * @return not locked Boolean (true)
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * UserDetails function
     * return true if user credentials is not expired, always true
     * @return not expired Boolean (true)
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * UserDetails function
     * return true if user is enabled, always true
     * @return enabled Boolean (true)
     */
    @Override
    public boolean isEnabled() {
        return true;
    }

    /**
     * UserDetails function
     * return true if object is equals to this, else return false
     * @param o Object
     * @return equals Boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || this.getClass() != o.getClass())
            return false;
        UserDetailsImpl user = (UserDetailsImpl) o;
        return Objects.equals(this.id, user.id);
    }
}
