package com.openclassrooms.mddapi.config;

import org.springframework.security.core.userdetails.UserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import io.jsonwebtoken.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JwtTokenUtil {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenUtil.class);

    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

    @Value("${jwt.secret}")
    private String secret;

    /**
     * With JWT token, get associated email
     * @param token String
     * @return email String
     */
    public String getEmailFromToken(String token) {
        return this.getClaimFromToken(token, Claims::getSubject);
    }

    /**
     * With JWT token, get its expiration date
     * @param token String
     * @return expiration date Date
     */
    public Date getExpirationDateFromToken(String token) {
        return this.getClaimFromToken(token, Claims::getExpiration);
    }

    /**
     * With JWT token and claims resolver, get claim
     * @param token String
     * @param claimsResolver Function<Claims,T>
     * @return claim <T>
     */
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = this.getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    /**
     * With JWT token, get all claims
     * @param token String
     * @return all claims Claims
     */
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token).getBody();
    }

    /**
     * With JWT token, return true if it is expired, else return false
     * @param token String
     * @return token expiration Boolean
     */
    private Boolean isTokenExpired(String token) {
        final Date expiration = this.getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    /**
     * With user details, get JWT token
     * @param userDetails UserDetails
     * @return token String
     */
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return this.doGenerateToken(claims, userDetails.getUsername());
    }

    /**
     * With claims and the subject (email), generate and get JWT token
     * @param claims Map<String,Object>
     * @param subject String
     * @return token String
     */
    private String doGenerateToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
            .signWith(SignatureAlgorithm.HS512, this.secret).compact();
    }

    /**
     * With JWT token, return true if it is validated, else return false and display error
     * @param token String
     * @param userDetails UserDetails
     * @return token validation Boolean
     */
    public Boolean validateToken(String token, UserDetails userDetails) {

        try {
            Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token);
            final String email = this.getEmailFromToken(token);
            return (email.equals(userDetails.getUsername()) && !this.isTokenExpired(token));
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            logger.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }
}
