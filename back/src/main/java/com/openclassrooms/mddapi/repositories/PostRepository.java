package com.openclassrooms.mddapi.repositories;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.openclassrooms.mddapi.models.Post;

@Repository
public interface PostRepository extends CrudRepository<Post, Long> {
    // all CRUD functions for Post
}
