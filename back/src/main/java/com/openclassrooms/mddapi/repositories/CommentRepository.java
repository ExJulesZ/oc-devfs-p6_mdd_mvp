package com.openclassrooms.mddapi.repositories;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.openclassrooms.mddapi.models.Comment;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {

    // all CRUD functions for Comment + findByPostId(id)

    /**
     * With a post id, if exists get associated comments list
     * @param id Long
     * @return comments list Iterable<Comment>
     */
    Iterable<Comment> findByPostId(Long id);
}
