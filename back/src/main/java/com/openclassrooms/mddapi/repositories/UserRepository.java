package com.openclassrooms.mddapi.repositories;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.openclassrooms.mddapi.models.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    // all CRUD functions for User + findByEmail(email) + existsByEmail(email)

    /**
     * With a user email, if exists get associated user
     * @param email String
     * @return user Optional<User>
     */
    Optional<User> findByEmail(String email);

    /**
     * With user email, return true if user exists, else return false
     * @param email String
     * @return user exists Boolean
     */
    Boolean existsByEmail(String email);
}
