package com.openclassrooms.mddapi.repositories;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.openclassrooms.mddapi.models.Topic;

@Repository
public interface TopicRepository extends CrudRepository<Topic, Long> {
    // all CRUD functions for Topic
}
