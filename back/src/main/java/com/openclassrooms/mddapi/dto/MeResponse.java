package com.openclassrooms.mddapi.dto;

import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

import com.openclassrooms.mddapi.models.Topic;

@AllArgsConstructor
@Getter
public class MeResponse {

    // sent information of logged user after get them

    private final Long id;

    private final String email;

    private final String username;

    private final String password;

    private final LocalDateTime createdAt;

    private final LocalDateTime updatedAt;

    private final List<Topic> topics;
}
