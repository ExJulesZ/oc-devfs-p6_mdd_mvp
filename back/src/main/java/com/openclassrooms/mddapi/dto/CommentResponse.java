package com.openclassrooms.mddapi.dto;

public class CommentResponse {

    // sent message after create (or not) a new comment on a post

    private final String message;

    public CommentResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
