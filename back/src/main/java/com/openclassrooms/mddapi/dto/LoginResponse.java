package com.openclassrooms.mddapi.dto;

public class LoginResponse {

    // generated and sent JWT token after log in

    private final String token;

    public LoginResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }
}
