package com.openclassrooms.mddapi.dto;

public class RegisterResponse {

    // sent message after register and thus create a new user

    private final String message;

    public RegisterResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
