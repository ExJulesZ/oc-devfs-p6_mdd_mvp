package com.openclassrooms.mddapi.dto;

import com.openclassrooms.mddapi.models.Post;

public class PostsResponse {

    // posts list received after get all existant posts

    private final Iterable<Post> posts;

    public PostsResponse(Iterable<Post> posts) {
        this.posts = posts;
    }

    public Iterable<Post> getPosts() {
        return this.posts;
    }
}
