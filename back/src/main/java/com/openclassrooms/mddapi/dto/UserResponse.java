package com.openclassrooms.mddapi.dto;

public class UserResponse {

    // sent message after update (or not) information of a user

    private final String message;

    public UserResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
