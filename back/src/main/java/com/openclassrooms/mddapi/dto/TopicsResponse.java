package com.openclassrooms.mddapi.dto;

import com.openclassrooms.mddapi.models.Topic;

public class TopicsResponse {

    // topics list received after get all existant topics

    private final Iterable<Topic> topics;

    public TopicsResponse(Iterable<Topic> topics) {
        this.topics = topics;
    }

    public Iterable<Topic> getTopics() {
        return this.topics;
    }
}
