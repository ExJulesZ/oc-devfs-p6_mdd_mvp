package com.openclassrooms.mddapi.dto;

import com.openclassrooms.mddapi.models.Comment;

public class CommentsResponse {

    // comments list received after get all comments of a post

    private final Iterable<Comment> comments;

    public CommentsResponse(Iterable<Comment> comments) {
        this.comments = comments;
    }

    public Iterable<Comment> getComments() {
        return this.comments;
    }
}
