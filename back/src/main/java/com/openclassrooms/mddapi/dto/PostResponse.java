package com.openclassrooms.mddapi.dto;

public class PostResponse {

    // sent message after create (or not) a new post

    private final String message;

    public PostResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
