package com.openclassrooms.mddapi.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.openclassrooms.mddapi.models.Topic;
import com.openclassrooms.mddapi.models.User;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostDto {

    // data to create a new post

    @NotBlank
    @Size(max = 128)
    private String title;

    @NotBlank
    @Size(max = 1024)
    private String content;

    @NotNull
    private Topic topic;

    @NotNull
    private User user;
}
