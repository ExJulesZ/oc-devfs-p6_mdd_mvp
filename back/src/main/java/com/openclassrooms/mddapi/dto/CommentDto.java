package com.openclassrooms.mddapi.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.openclassrooms.mddapi.models.Post;
import com.openclassrooms.mddapi.models.User;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDto {

    // data to create a new comment on a post

    @NotBlank
    @Size(max = 1024)
    private String content;

    @NotNull
    private Post post;

    @NotNull
    private User user;
}
