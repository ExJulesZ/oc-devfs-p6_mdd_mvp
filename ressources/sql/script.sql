-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 20 déc. 2023 à 18:53
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `mdd_mvp`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` varchar(1024) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post` (`post_id`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`id`, `content`, `post_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Merci pour cet article. Il est très intéressant !', 3, 1, '2023-11-25 22:40:12', '2023-11-25 22:40:12'),
(2, 'Vive la POO !!', 5, 1, '2023-11-25 22:40:12', '2023-11-25 22:40:12'),
(3, 'Merci pour ton message Jules !', 3, 2, '2023-11-25 22:45:30', '2023-11-25 22:45:30');

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `content` varchar(1024) NOT NULL,
  `topic_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `topic` (`topic_id`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `topic_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Angular : qu’est-ce que c’est et pourquoi l’utiliser ?', 'Si vous vous intéressez au développement de produits numériques, notamment les applications web, vous avez sans doute déjà entendu parler du framework Angular. Très populaire, il est utilisé par de grandes entreprises comme Google ou encore PayPal. Mais ce n’est pas tout d’avoir un outil reconnu, encore faut-il savoir s’il correspond à vos besoins. Avant de vous en dire plus sur Angular, vous devez d’abord comprendre ce qu’est un framework front-end. Ce type d’infrastructure permet d’accélérer et de faciliter le développement et la maintenance d’une application web ou mobile. Comment ? En fournissant un cadre, des composants et du code réutilisable. Grâce à cela, les développeurs peuvent se passer de créer toute une structure de A à Z pour une application avec toutes les normes de développement associées. Vous n’avez plus besoin de travailler sur une application personnalisée, il est possible d’utiliser une structure, et de reprendre les composants et codes déjà existants.', 1, 1, '2023-11-25 22:37:14', '2023-11-25 22:37:14'),
(2, 'Débutez avec Angular', 'En bref, Angular est un framework JavaScript qui vous permet de développer des applications \"efficaces et sophistiquées\", comme dit la documentation. Il permet notamment de créer ce qu\'on appelle des Single Page Applications (ou SPA) : des applications entières qui tournent dans une seule page HTML grâce au JavaScript. Un framework logiciel est un ensemble d\'outils et de composants à partir desquels on peut développer des applications. Le développement Angular passe par trois langages principaux : le HTML pour structurer – toutes vos connaissances avec ce langage vous seront utiles, et Angular viendra vous ajouter quelques nouveautés ; le SCSS pour les styles – le SCSS est une surcouche du CSS qui y apporte des fonctionnalités supplémentaires, mais qui permet également d\'écrire du CSS pur si on le souhaite ; le TypeScript pour tout ce qui est dynamique, comportement et données – un peu comme le JavaScript sur un site sans framework.', 1, 1, '2023-11-25 22:37:14', '2023-11-25 22:37:14'),
(3, 'Java : l’essentiel à savoir sur le langage de programmation', 'Java est un langage de programmation orienté objet qui produit des logiciels pour plusieurs plateformes. Lorsqu’un programmeur écrit une application Java, le code compilé (appelé bytecode) s’exécute sur la plupart des systèmes d’exploitation (OS), y compris Windows, Linux et Mac OS. Java tire une grande partie de sa syntaxe des langages de programmation C et C++. La plate-forme Java (l’environnement dans lequel un programme s’exécute) se distingue du fait qu’elle s’exécute sur d’autres plateformes matérielles. Elle comporte deux composants : la machine virtuelle Java (Java VM) et l’interface de programmation d’applications Java (API Java). Java a été développée au milieu des années 1990 par James A. Gosling, un ancien informaticien de Sun Microsystems, avec Mike Sheridan et Patrick Naughton.', 2, 2, '2023-11-25 22:37:14', '2023-11-25 22:37:14'),
(4, '5 bonnes pratiques à adopter pour faire du JAVA ', 'Il y a plusieurs bonnes pratiques à suivre lors de la programmation en Java : Utiliser des noms de variables et de méthodes descriptifs : Les noms de variables et de méthodes doivent être clairs et décrire leur fonction ; Utiliser des commentaires judicieusement : Les commentaires doivent être utilisés pour expliquer les sections de code qui peuvent être difficiles à comprendre ; Utiliser des exceptions plutôt que des valeurs de retour pour signaler les erreurs : Les exceptions sont plus faciles à gérer et à suivre lorsque les erreurs se produisent ; Utiliser des collections plutôt que des tableaux : Les collections de Java offrent des fonctionnalités supplémentaires par rapport aux tableaux, comme une taille dynamique et des itérateurs ; Utiliser des modificateurs de visibilité appropriés : Les modificateurs de visibilité tels que « public », « private » et « protected » doivent être utilisés pour contrôler l’accès aux membres de la classe.', 2, 2, '2023-11-25 22:37:14', '2023-11-25 22:37:14'),
(5, 'POO : Le guide ultime', 'Le langage de programmation Python est un langage très accessible pour commencer la programmation. Grâce à ses librairies Numpy, Pandas, Scikit-learn et Matplotlib, il est aussi un outil formidable pour les Data Scientists. La facilité avec laquelle Python peut être abordée vient notamment de ses multiples paradigmes: Python peut être utilisé de manière impérative, en suivant un paradigme fonctionnel ou en implémentant les règles de la Programmation Objet. Dans ce dossier, nous allons définir la programmation orientée objet, ses grands principes et particularismes et son utilisation en Python. Nous verrons en quoi cette notion est très importante pour les Data Scientists comme pour les Data Engineers sans être pour autant trop difficile d’accès.', 3, 2, '2023-11-25 22:37:14', '2023-11-25 22:37:14');

-- --------------------------------------------------------

--
-- Structure de la table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `topic_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  KEY `topic` (`topic_id`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `subscriptions`
--

INSERT INTO `subscriptions` (`topic_id`, `user_id`) VALUES
(2, 2),
(3, 2),
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `topics`
--

DROP TABLE IF EXISTS `topics`;
CREATE TABLE IF NOT EXISTS `topics` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  `description` varchar(512) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `topics`
--

INSERT INTO `topics` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Angular', 'Angular est un framework pour clients, open source, basé sur TypeScript et codirigé par l\'équipe du projet « Angular » chez Google ainsi que par une communauté de particuliers et de sociétés. Angular est une réécriture complète d\'AngularJS, cadriciel construit par la même équipe.', '2023-11-25 22:34:35', '2023-11-25 22:34:35'),
(2, 'Java', 'Java est un langage de programmation de haut niveau orienté objet créé par James Gosling et Patrick Naughton, employés de Sun Microsystems, avec le soutien de Bill Joy, présenté officiellement le 23 mai 1995 au SunWorld. La société Sun est rachetée en 2009 par la société Oracle qui détient et maintient désormais Java.', '2023-11-25 22:34:35', '2023-11-25 22:34:35'),
(3, 'POO', 'La programmation orientée objet, ou programmation par objet, est un paradigme de programmation informatique.', '2023-11-25 22:34:35', '2023-11-25 22:34:35');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`email`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'jules.grelet@live.fr', 'JulesG', '$2y$10$hBS5CBXCmbybMJL9pmtxVeU1UNAbqnxxtqSzoi.7AI3eco7DlyqXq', '2023-11-25 22:36:29', '2023-12-15 10:12:30'),
(2, 'jean.dupond@outlook.com', 'JeanDupond', '$2y$10$hBS5CBXCmbybMJL9pmtxVeU1UNAbqnxxtqSzoi.7AI3eco7DlyqXq', '2023-11-25 22:36:29', '2023-11-25 22:36:29');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `FK8omq0tc18jd43bu5tjh6jvraq` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FKh4c7lvsc298whoyd4w9ta25cr` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);

--
-- Contraintes pour la table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `FK5lidm6cqbc7u4xhqpxm898qme` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FKrfchr8dax0kfngvvkbteh5n7h` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`);

--
-- Contraintes pour la table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD CONSTRAINT `FKfuag8et2vdg3ds9h8trqx2ldq` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`),
  ADD CONSTRAINT `FKhro52ohfqfbay9774bev0qinr` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
