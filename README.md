# P6 : MDD MVP



## I - Mise en place de l’application


### 1. Récupérez le projet avec `git clone https://gitlab.com/ExJulesZ/oc-devfs-p6_mdd_mvp` à l'emplacement de votre choix.


### 2. Installation de la base de données

1. Démarrez Wamp (ou équivalent) ;

2. Dans phpMyAdmin ou en ligne de commande, créez une nouvelle base de données nommée **mdd_mvp** ;

3. Utilisez le script sql fourni (**oc-devfs-p6_mdd_mvp/ressources/sql/script.sql**) contenant le schéma et des données de départ.


### 3. Installation de l’application

1. Ouvrez le répertoire **back** avec IntelliJ (ou autre IDE) ;

2. Le nécessaire sera téléchargé automatiquement, mais si besoin ouvrez **pom.xml** et cliquez sur le bouton **Load Maven Changes** ou faites **Ctrl+Maj+O** ;

3. Accédez à **front** avec `cd front` ;

4. Installez toutes les dépendances nécessaires avec `npm i`.


### 4. Lancement de l’application

1. Dans IntelliJ, ajoutez les variables d'environnement suivantes : `APP_DB_HOST=127.0.0.1;APP_DB_NAME=MDD_MVP;APP_DB_PASS=p@ssw0rd;APP_DB_PORT=3306;APP_DB_USER=springuser`

2. Dans IntelliJ, faites un click-droit sur **MddApiApplication.java** et cliquez sur **Run ...** pour démarrer l'API ;

3. Au premier lancement l'application back-end doit build. Dans ce cas recommencez le point précédent ;

4. Accédez à **front** avec `cd front` ;

5. Démarrez l'application front-end avec `ng serve` ;

6. Une fois l'application lancée, suivez le lien indiqué. Cela devrait être http://localhost:4200/ ;

7. Pour se connecter : cliquez sur **Se connecter**, entrez **jules.grelet@live.fr** et **Password1234!** (données de départ), et cliquez sur **Se connecter**.
