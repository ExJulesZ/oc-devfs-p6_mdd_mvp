import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { SessionService } from 'src/app/auth/services/session.service';
import { UserRequest } from 'src/app/interfaces/dto/userRequest.interface';
import { User } from 'src/app/interfaces/user.interface';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-me',
  templateUrl: './me.component.html',
  styleUrls: ['./me.component.scss']
})
export class MeComponent {

  public me: User|undefined;
  public meForm: FormGroup|undefined;
  public meFormInvalid: boolean|undefined;
  public loginInfo: boolean = false;
  public submitDisabled: boolean = false;

  constructor(
    private router: Router, 
    public userService: UserService, 
    public authService: AuthService, 
    public sessionService: SessionService, 
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    const user = this.sessionService.user;
    if(user) this.me = user;
    else this.router.navigate(['']);
    this.initForm();
  }

  private initForm(): void {
    let username = this.me?.username;
    let email = this.me?.email;
    if(!username) username = "";
    if(!email) email = "";
    this.meForm = this.fb.group({
      meUsername: [ username , [Validators.required, Validators.maxLength(64)] ], 
      meEmail: [ email , [Validators.required, Validators.maxLength(64), Validators.email] ]
    });
    this.meFormInvalid = true;
  }

  public fieldChange(): void {
    const username = this.meForm?.value.meUsername;
    const email = this.meForm?.value.meEmail;

    if(email!=this.me?.email) this.loginInfo = true;
    else this.loginInfo = false;

    if(!this.meFormInvalid && username==this.me?.username && email==this.me?.email) this.meFormInvalid = true;
    else if(this.meFormInvalid) this.meFormInvalid = false;
  }

  public submit(): void {
    if(this.me){
      const email = this.meForm?.value.meEmail;
      const username = this.meForm?.value.meUsername;
      if(email!=this.me?.email || username!=this.me?.username){
        this.submitDisabled = true;
        const user = this.me;
        const userRequest: UserRequest = {
          email: email, 
          username: username
        };
        const currentRoute = this.router.url;
        this.userService.update(user.id.toString(), userRequest).subscribe(() => {
          this.updateMe(email, user.email, username);
          this.router.navigateByUrl('/',{skipLocationChange:true}).then(() => {
            this.router.navigate([currentRoute]);
          });
        }, error => this.submitDisabled = false );
      }
    }
  }

  private updateMe(email:string, myEmail:string, username:string): void {
    if(email!=myEmail){
      this.sessionService.logOut();
      this.router.navigate(['']);
    } else {
      this.sessionService.user!.username = username;
    }
  }

  public logout(): void {
    this.sessionService.logOut();
    this.router.navigate(['']);
  }

  public unsubscribe(id:number): void {
    if(this.me){
      const userId = this.me.id.toString();
      const topicId = id.toString();
      const currentRoute = this.router.url;
      this.userService.unsubscribe(userId, topicId).subscribe(() => {
        this.authService.me().subscribe((user: User) => {
          this.sessionService.user!.topics = user.topics;
        });
        this.router.navigateByUrl('/',{skipLocationChange:true}).then(() => {
          this.router.navigate([currentRoute]);
        });
      });
    }
  }
}
