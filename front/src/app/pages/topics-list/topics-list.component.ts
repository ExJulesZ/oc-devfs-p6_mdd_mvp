import { Component, OnInit } from '@angular/core';
import { TopicService } from 'src/app/services/topic.service';
import { Observable } from 'rxjs';
import { TopicsResponse } from 'src/app/interfaces/dto/topicsResponse.interface';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/interfaces/user.interface';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/auth/services/session.service';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Topic } from 'src/app/interfaces/topic.interface';

@Component({
  selector: 'app-topics-list',
  templateUrl: './topics-list.component.html',
  styleUrls: ['./topics-list.component.scss']
})
export class TopicsListComponent implements OnInit {

  public topics$: Observable<TopicsResponse>|undefined;
  public me: User|undefined;
  public myTopicsId: number[] = [];

  constructor(
    private router: Router, 
    private topicService: TopicService, 
    private userService: UserService, 
    private authService: AuthService, 
    private sessionService: SessionService
  ) {}

  ngOnInit(): void {
    this.topics$ = this.topicService.getAll();
    const user = this.sessionService.user;
    if(user) this.me = user;
    else this.router.navigate(['']);
    this.setMyTopicsId(this.me?.topics,false);
  }

  private setMyTopicsId(topics:Topic[]|undefined,subscribe:boolean): void {
    if(topics){
      this.myTopicsId = [];
      topics.forEach(topic => this.myTopicsId?.push(topic.id));
      if(subscribe){
        const currentRoute = this.router.url;
        this.router.navigateByUrl('/',{skipLocationChange:true}).then(() => {
          this.router.navigate([currentRoute]);
        });
      }
    }
  }

  public subscribe(id:number): void {
    if(this.me){
      const userId = this.me.id.toString();
      const topicId = id.toString();
      this.userService.subscribe(userId, topicId).subscribe(() => {
        this.authService.me().subscribe((user: User) => {
          this.sessionService.user!.topics = user.topics;
          this.setMyTopicsId(user.topics,true);
        });
      });
    }
  }
}
