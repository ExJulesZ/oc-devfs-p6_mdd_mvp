import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { SessionService } from 'src/app/auth/services/session.service';
import { RegisterRequest } from 'src/app/interfaces/dto/registerRequest.interface';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup|undefined;
  public passwordPattern: string = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$";
  public passwordHidden: boolean = true;
  public onError: boolean = false;
  public submitDisabled: boolean = false;

  constructor(
    private authService: AuthService, 
    private fb: FormBuilder, 
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.registerForm = this.fb.group({
      registerUsername: [ "" , [Validators.required, Validators.maxLength(64)] ], 
      registerEmail: [ "" , [Validators.required, Validators.maxLength(64), Validators.email] ], 
      registerPassword: [ "" , [Validators.required, Validators.minLength(8), Validators.maxLength(128)] ]
    });
  }

  public submit(): void {
    this.submitDisabled = true;
    const registerRequest: RegisterRequest = {
      email: this.registerForm?.value.registerEmail, 
      username: this.registerForm?.value.registerUsername, 
      password: this.registerForm?.value.registerPassword
    }
    this.authService.register(registerRequest).subscribe(() => {
      this.router.navigate(['login'])
    },
    error => {
      this.onError = true;
      this.submitDisabled = false;
    });
  }
}
