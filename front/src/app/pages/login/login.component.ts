import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { SessionService } from 'src/app/auth/services/session.service';
import { AuthResponse } from 'src/app/interfaces/dto/authResponse.interface';
import { LoginRequest } from 'src/app/interfaces/dto/loginRequest.interface';
import { User } from 'src/app/interfaces/user.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup|undefined;
  public passwordHidden: boolean = true;
  public onError: boolean = false;
  public submitDisabled: boolean = false;

  constructor(
    private authService: AuthService, 
    private sessionService: SessionService, 
    private fb: FormBuilder, 
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.loginForm = this.fb.group({
      loginEmail: [ "" , [Validators.required, Validators.maxLength(64), Validators.email] ], 
      loginPassword: [ "" , [Validators.required, Validators.maxLength(128)] ]
    });
  }

  public submit(): void {
    this.submitDisabled = true;
    const loginRequest: LoginRequest = {
      email: this.loginForm?.value.loginEmail, 
      password: this.loginForm?.value.loginPassword
    }
    this.authService.login(loginRequest).subscribe(
      (authResponse: AuthResponse) => {
        this.sessionService.token = authResponse.token;
        this.authService.me().subscribe((user: User) => {
          this.sessionService.logIn(user);
          this.router.navigate(['/posts'])
        });
        this.router.navigate(['/posts'])
      },
      error => {
        this.onError = true;
        this.submitDisabled = false;
      }
    );
  }
}
