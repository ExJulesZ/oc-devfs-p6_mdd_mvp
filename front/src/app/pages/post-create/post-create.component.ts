import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostService } from 'src/app/services/post.service';
import { TopicService } from 'src/app/services/topic.service';
import { Observable } from 'rxjs';
import { TopicsResponse } from 'src/app/interfaces/dto/topicsResponse.interface';
import { PostRequest } from 'src/app/interfaces/dto/postRequest.interface';
import { User } from 'src/app/interfaces/user.interface';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/auth/services/session.service';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss']
})
export class PostCreateComponent implements OnInit {

  public topics$: Observable<TopicsResponse>|undefined;
  public me: User|undefined;
  public postForm: FormGroup|undefined;
  public onError: boolean = false;
  public submitDisabled: boolean = false;

  constructor(
    private router: Router, 
    private postService: PostService, 
    private topicService: TopicService, 
    private sessionService: SessionService, 
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.topics$ = this.topicService.getAll();
    const user = this.sessionService.user;
    if(user) this.me = user;
    else this.router.navigate(['']);
    this.initForm();
  }

  private initForm(): void {
    this.postForm = this.fb.group({
      postTopic: [ null , [Validators.required] ], 
      postTitle: [ "" , [Validators.required, Validators.maxLength(128)] ], 
      postContent: [ "" , [Validators.required, Validators.maxLength(1024)] ]
    });
  }

  public submit(): void {
    if(this.me){
      this.submitDisabled = true;
      const postRequest: PostRequest = {
        title: this.postForm?.value.postTitle, 
        content: this.postForm?.value.postContent, 
        topic: this.postForm?.value.postTopic, 
        user: this.me
      }
      this.postService.create(postRequest).subscribe(() => {
        this.router.navigate(["posts"]);
      }, error => {
        this.onError = true;
        this.submitDisabled = false;
      } );
    }
  }
}
