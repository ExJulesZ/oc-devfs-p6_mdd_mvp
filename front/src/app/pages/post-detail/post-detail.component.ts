import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from 'src/app/auth/services/session.service';
import { CommentRequest } from 'src/app/interfaces/dto/commentRequest.interface';
import { CommentsResponse } from 'src/app/interfaces/dto/commentsResponse.interface';
import { Post } from 'src/app/interfaces/post.interface';
import { User } from 'src/app/interfaces/user.interface';
import { CommentService } from 'src/app/services/comment.service';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {

  public id!: string;
  public post: Post|undefined;
  public comments$: Observable<CommentsResponse>|undefined;
  public me: User|undefined;
  public commentForm: FormGroup|undefined;
  public submitDisabled: boolean = false;

  constructor(
    private router: Router, 
    private route: ActivatedRoute, 
    private postService: PostService, 
    private commentService: CommentService, 
    private sessionService: SessionService, 
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id')!;
    this.postService.getById(this.id).subscribe(
      (post: Post) => {
        if(post){
          this.post = post;
          this.comments$ = this.commentService.getAllByPost(this.post.id.toString());
        }
        else{
          this.router.navigate(["posts"]);
        }
      }
    );
    const user = this.sessionService.user;
    if(user) this.me = user;
    else this.router.navigate(['']);
    this.initForm();
  }

  private initForm(): void {
    this.commentForm = this.fb.group({
      commentContent: [ "" , [Validators.required, Validators.maxLength(1024)] ]
    });
  }

  public submit(): void {
    if(this.me){
      this.submitDisabled = true;
      const commentRequest: CommentRequest = {
        content: this.commentForm?.value.commentContent,
        post: this.post!,
        user: this.me
      };
      const currentRoute = this.router.url;
      this.commentService.create(commentRequest).subscribe(() => {
        this.router.navigateByUrl('/',{skipLocationChange:true}).then(() => {
          this.router.navigate([currentRoute]);
        });
      }, error => this.submitDisabled = false );
    }
  }
}
