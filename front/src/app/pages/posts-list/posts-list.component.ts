import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import { Observable } from 'rxjs';
import { PostsResponse } from 'src/app/interfaces/dto/postsResponse.interface';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss']
})
export class PostsListComponent implements OnInit {

  public posts$: Observable<PostsResponse>|undefined;

  constructor(
    private postService: PostService
  ) {}

  ngOnInit(): void {
    this.posts$ = this.postService.getAll();
  }

}
