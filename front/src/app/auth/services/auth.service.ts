import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthResponse } from "src/app/interfaces/dto/authResponse.interface";
import { LoginRequest } from "src/app/interfaces/dto/loginRequest.interface";
import { RegisterRequest } from "src/app/interfaces/dto/registerRequest.interface";
import { User } from "src/app/interfaces/user.interface";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private pathService = 'api/auth';

    constructor(private httpClient: HttpClient) { }

    public login(loginRequest: LoginRequest): Observable<AuthResponse> {
        return this.httpClient.post<AuthResponse>(`${this.pathService}/login`, loginRequest);
    }

    public register(registerRequest: RegisterRequest): Observable<AuthResponse> {
        return this.httpClient.post<AuthResponse>(`${this.pathService}/register`, registerRequest);
    }

    public me(): Observable<User> {
        return this.httpClient.get<User>(`${this.pathService}/me`);
    }
}