import { Post } from "./post.interface";
import { User } from "./user.interface";

export interface Comment {
	id: number, 
	content: string, 
	post: Post, 
	user: User, 
	createdAt: Date, 
	updatedAt: Date
}
