import { Post } from "../post.interface";
import { User } from "../user.interface";

export interface CommentRequest {
	content: string;
    post: Post
    user: User;
}
