import { Topic } from "../topic.interface";
import { User } from "../user.interface";

export interface PostRequest {
	title: string;
    content: string;
    topic: Topic;
    user: User;
}
