import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { PostsListComponent } from './pages/posts-list/posts-list.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { PostDetailComponent } from './pages/post-detail/post-detail.component';
import { PostCreateComponent } from './pages/post-create/post-create.component';
import { TopicsListComponent } from './pages/topics-list/topics-list.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { MeComponent } from './pages/me/me.component';
import { UnauthGuard } from './auth/guards/unauth.guard';
import { AuthGuard } from './auth/guards/auth.guard';

// consider a guard combined with canLoad / canActivate route option
// to manage unauthenticated user to access private routes
const routes: Routes = [
  // Unauth routes
  { 
    path: '', 
    canActivate: [UnauthGuard], 
    component: HomeComponent 
  }, 
  { 
    path: 'home', 
    canActivate: [UnauthGuard], 
    component: HomeComponent 
  }, 
  { 
    path: 'login', 
    canActivate: [UnauthGuard], 
    component: LoginComponent 
  }, 
  { 
    path: 'register', 
    canActivate: [UnauthGuard], 
    component: RegisterComponent 
  }, 

  // Auth routes
  {
    path: 'posts', 
    canActivate: [AuthGuard], 
    component: PostsListComponent
  }, 
  {
    path: 'posts/list', 
    canActivate: [AuthGuard], 
    component: PostsListComponent
  }, 
  {
    path: 'posts/detail/:id', 
    canActivate: [AuthGuard], 
    component: PostDetailComponent
  }, 
  {
    path: 'posts/create', 
    canActivate: [AuthGuard], 
    component: PostCreateComponent
  }, 
  {
    path: 'topics', 
    canActivate: [AuthGuard], 
    component: TopicsListComponent
  }, 
  {
    path: 'topics/list', 
    canActivate: [AuthGuard], 
    component: TopicsListComponent
  }, 
  {
    path: 'me', 
    canActivate: [AuthGuard], 
    component: MeComponent
  }, 

  // Error routes
  { path: '404', component: NotFoundComponent }, 
  { path: '**', redirectTo: '404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
