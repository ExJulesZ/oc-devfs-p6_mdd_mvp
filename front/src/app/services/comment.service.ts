import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { CommentsResponse } from "../interfaces/dto/commentsResponse.interface";
import { CommentResponse } from "../interfaces/dto/commentResponse.interface";
import { CommentRequest } from "../interfaces/dto/commentRequest.interface";

@Injectable({
    providedIn: 'root'
})
export class CommentService {

    private pathService = 'api/comments';

    constructor(private httpClient: HttpClient) {}

    public create(commentRequest: CommentRequest): Observable<CommentResponse> {
        return this.httpClient.post<CommentResponse>(`${this.pathService}`, commentRequest);
    }

    public getAllByPost(id: string): Observable<CommentsResponse> {
        return this.httpClient.get<CommentsResponse>(`${this.pathService}/post/${id}`);
    }
}
