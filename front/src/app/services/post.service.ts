import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PostsResponse } from "../interfaces/dto/postsResponse.interface";
import { Post } from "../interfaces/post.interface";
import { PostResponse } from "../interfaces/dto/postResponse.interface";
import { PostRequest } from "../interfaces/dto/postRequest.interface";

@Injectable({
    providedIn: 'root'
})
export class PostService {

    private pathService = 'api/posts';

    constructor(private httpClient: HttpClient) {}

    public create(postRequest: PostRequest): Observable<PostResponse> {
        return this.httpClient.post<PostResponse>(`${this.pathService}`, postRequest);
    }

    public getAll(): Observable<PostsResponse> {
        return this.httpClient.get<PostsResponse>(`${this.pathService}`);
    }

    public getById(id: string): Observable<Post> {
        return this.httpClient.get<Post>(`${this.pathService}/${id}`);
    }
}
