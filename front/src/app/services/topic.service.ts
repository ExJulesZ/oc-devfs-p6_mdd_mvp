import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { TopicsResponse } from "../interfaces/dto/topicsResponse.interface";

@Injectable({
    providedIn: 'root'
})
export class TopicService {

    private pathService = 'api/topics';

    constructor(private httpClient: HttpClient) {}

    public getAll(): Observable<TopicsResponse> {
        return this.httpClient.get<TopicsResponse>(`${this.pathService}`);
    }
}