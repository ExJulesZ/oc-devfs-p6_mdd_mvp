import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { TopicsResponse } from "../interfaces/dto/topicsResponse.interface";
import { UserResponse } from "../interfaces/dto/userResponse.interface";
import { UserRequest } from "../interfaces/dto/userRequest.interface";

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private pathService = 'api/users';

    constructor(private httpClient: HttpClient) {}

    public getAll(): Observable<TopicsResponse> {
        return this.httpClient.get<TopicsResponse>(`${this.pathService}`);
    }

    public update(id:string, userRequest: UserRequest): Observable<UserResponse> {
        return this.httpClient.put<UserResponse>(`${this.pathService}/${id}`, userRequest);
    }

    public subscribe(userId: string, topicId: string): Observable<void> {
        return this.httpClient.post<void>(`${this.pathService}/${userId}/topic/${topicId}/subscribe`, null);
    }

    public unsubscribe(userId: string, topicId: string): Observable<void> {
        return this.httpClient.delete<void>(`${this.pathService}/${userId}/topic/${topicId}/unsubscribe`);
    }
}
